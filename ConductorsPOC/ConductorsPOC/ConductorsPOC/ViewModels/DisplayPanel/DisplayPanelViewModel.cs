﻿using Caliburn.Micro;
using Common.Enums;
using ConductorsPOC.Messages.Messages;

namespace ConductorsPOC.ViewModels.DisplayPanel
{
    public class DisplayPanelViewModel : ViewModelBase, IHandle<ScreenStateMessage>
    {
        public string TabAContainerStateLabel { get; set; }
        public string TabALeftPanelStateLabel { get; set; }
        public string TabARightPanelStateLabel { get; set; }
        public string TabBContainerStateLabel { get; set; }
        public string AdditionalWindowStateLabel { get; set; }


        public DisplayPanelViewModel()
        {
            SetDefaultValues();
        }

        private void SetDefaultValues()
        {
            AdditionalWindowStateLabel = ScreenStateEnum.Inactive.ToString();
            TabAContainerStateLabel =  ScreenStateEnum.Inactive.ToString(); 
            TabALeftPanelStateLabel = ScreenStateEnum.Inactive.ToString();
            TabARightPanelStateLabel = ScreenStateEnum.Inactive.ToString();
            TabBContainerStateLabel = ScreenStateEnum.Inactive.ToString();
        }

        public void Handle(ScreenStateMessage message)
        {
            switch (message.ScreenControl)
            {
                case ScreenControlEnum.TabAContainer:
                    TabAContainerStateLabel = message.ScreenState.ToString();
                    NotifyOfPropertyChange(() => TabAContainerStateLabel); 
                    break;

                case ScreenControlEnum.TabALeftPanel:
                    TabALeftPanelStateLabel = message.ScreenState.ToString();
                    NotifyOfPropertyChange(() => TabALeftPanelStateLabel);
                    break;

                case ScreenControlEnum.TabARightPanel:
                    TabARightPanelStateLabel = message.ScreenState.ToString();
                    NotifyOfPropertyChange(() => TabARightPanelStateLabel);
                    break;

                case ScreenControlEnum.TabBContainer:
                    TabBContainerStateLabel = message.ScreenState.ToString();
                    NotifyOfPropertyChange(() => TabBContainerStateLabel);
                    break;

                case ScreenControlEnum.AdditionalWindow:
                    AdditionalWindowStateLabel = message.ScreenState.ToString();
                    NotifyOfPropertyChange(() => AdditionalWindowStateLabel);
                    break;
            }
        }

        //This class has only custom implementation of OnInitialize method, 
        //since in this case we don't need more than default implementation 
        //of OnActivate and OnDeactivate to manage its lifecycle
        protected override void OnInitialize()
        {
            base.OnInitialize();
            GetEventAggregator().Subscribe(this);
        }

    }
}
