﻿using Common.Enums;

namespace ConductorsPOC.ViewModels.TabA
{
    public class LeftPanelViewModel : ViewModelBase
    {
        protected override void OnActivate()
        {
            PublishScreenStateChange(ScreenControlEnum.TabALeftPanel, true);
        }

        protected override void OnDeactivate(bool close)
        {
            PublishScreenStateChange(ScreenControlEnum.TabALeftPanel, false);
        }
    }
}
