﻿using Common.Enums;

namespace ConductorsPOC.ViewModels.TabA
{
    public class TabAViewModel : ViewModelBase
    {
        public LeftPanelViewModel LeftPanelVM { get; set; }
        public RightPanelViewModel RightPanelVM { get; set; }


        public TabAViewModel()
        {
            InitializeViewModels();
            NotifyOfViewModelsChanged();
        }

        private void InitializeViewModels()
        {
            LeftPanelVM = new LeftPanelViewModel();
            RightPanelVM = new RightPanelViewModel();
        }

        private void NotifyOfViewModelsChanged()
        {
            NotifyOfPropertyChange(() => LeftPanelVM);
            NotifyOfPropertyChange(() => RightPanelVM);
        }


        protected override void OnActivate()
        {
            ActivateItem(LeftPanelVM);
            ActivateItem(RightPanelVM);

            PublishScreenStateChange(ScreenControlEnum.TabAContainer, true);
        }

        protected override void OnDeactivate(bool close)
        {
            DeactivateItem(LeftPanelVM, true);
            DeactivateItem(RightPanelVM, true);

            PublishScreenStateChange(ScreenControlEnum.TabAContainer, false);
        }
    }
}
