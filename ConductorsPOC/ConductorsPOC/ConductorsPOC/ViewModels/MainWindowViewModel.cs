﻿using ConductorsPOC.ViewModels.AdditionalWindow;
using ConductorsPOC.ViewModels.DisplayPanel;
using ConductorsPOC.ViewModels.TabA;
using ConductorsPOC.ViewModels.TabB;

namespace ConductorsPOC.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        public ViewModelBase ActiveTab { get; set; }
        public DisplayPanelViewModel DisplayPanelVM { get; set; }


        public MainWindowViewModel()
        {
            DisplayName = "Conductors POC"; //Sets window title (Caliburn convention)
        }

        private void InitializeDisplayPanel()
        {
            DisplayPanelVM = new DisplayPanelViewModel();
            NotifyOfPropertyChange(() => DisplayPanelVM);
        }

        public void OpenAdditionalWindowButton()
        {
            DeactivateItem(ActiveTab, true);

            var additionalWindowVM = new AdditionalWindowViewModel();

            ActivateItem(additionalWindowVM);
            GetWindowManager().ShowDialog(additionalWindowVM);

            ActivatePanel(ActiveTab);
        }

        public void OpenTabAButton()
        {
            ActivatePanel(new TabAViewModel());
        }

        public void OpenTabBButton()
        {
            ActivatePanel(new TabBViewModel());
        }

        /// <summary>
        /// Deactivates previously active control, assigns passed ViewModel to binded property, to display it on the main screen, then activates new control.
        /// </summary>
        /// <param name="panel"></param>
        private void ActivatePanel(ViewModelBase panel)
        {
            DeactivateItem(ActiveTab, true);

            ActiveTab = panel;
            NotifyOfPropertyChange(() => ActiveTab);

            ActivateItem(ActiveTab);
        }


        //In this case we don't want DisplayPanelVM to be deactivated, because it displays screen statuses
        private void CheckIfDisplayPanelIsActive()
        {
            if (DisplayPanelVM != null && !DisplayPanelVM.IsActive)
            {
                ActivateItem(DisplayPanelVM);
            }
        }

        protected override void OnActivate()
        {
            InitializeDisplayPanel();
            CheckIfDisplayPanelIsActive();
        }

        protected override void OnDeactivate(bool close)
        {
            DeactivateItem(DisplayPanelVM, true);
            DeactivateItem(ActiveTab, true);
        }

    }
}
