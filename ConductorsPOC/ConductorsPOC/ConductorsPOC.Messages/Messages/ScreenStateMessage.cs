﻿using Common.Enums;

namespace ConductorsPOC.Messages.Messages
{
    public class ScreenStateMessage
    {
        public ScreenControlEnum ScreenControl { get; set; }
        public ScreenStateEnum ScreenState { get; set; }
    }
}
