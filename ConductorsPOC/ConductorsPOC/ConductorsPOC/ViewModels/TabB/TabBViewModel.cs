﻿using Common.Enums;

namespace ConductorsPOC.ViewModels.TabB
{
    public class TabBViewModel : ViewModelBase
    {
        protected override void OnActivate()
        {
            PublishScreenStateChange(ScreenControlEnum.TabBContainer, true);
        }

        protected override void OnDeactivate(bool close)
        {
            PublishScreenStateChange(ScreenControlEnum.TabBContainer, false);
        }
    }
}
