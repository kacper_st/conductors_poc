﻿using Common.Enums;

namespace ConductorsPOC.ViewModels.TabA
{
    public class RightPanelViewModel : ViewModelBase
    {
        protected override void OnActivate()
        {
            PublishScreenStateChange(ScreenControlEnum.TabARightPanel, true);
        }

        protected override void OnDeactivate(bool close)
        {
            PublishScreenStateChange(ScreenControlEnum.TabARightPanel, false);
        }
    }
}
