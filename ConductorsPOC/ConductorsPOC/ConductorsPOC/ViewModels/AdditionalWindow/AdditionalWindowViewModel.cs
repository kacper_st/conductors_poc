﻿using Common.Enums;

namespace ConductorsPOC.ViewModels.AdditionalWindow
{
    public class AdditionalWindowViewModel : ViewModelBase
    {

        public AdditionalWindowViewModel()
        {
            DisplayName = "Conductors POC - Additional window";
        }


        protected override void OnActivate()
        {
            PublishScreenStateChange(ScreenControlEnum.AdditionalWindow, true);
        }

        protected override void OnDeactivate(bool close)
        {
            PublishScreenStateChange(ScreenControlEnum.AdditionalWindow, false);
        }
    }
}
