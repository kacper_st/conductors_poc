﻿using Caliburn.Micro;
using Common.Enums;
using ConductorsPOC.Messages.Messages;

namespace ConductorsPOC.ViewModels
{
    public class ViewModelBase : Conductor<ViewModelBase>.Collection.AllActive
    {
        private static IEventAggregator eventAggregator;
        private static WindowManager windowManager;

        /// <summary>
        /// Returns instance of shared EventAggregator.
        /// </summary>
        /// <returns></returns>
        public static IEventAggregator GetEventAggregator()
        {
            return eventAggregator ?? (eventAggregator = new EventAggregator());
        }


        /// <summary>
        /// Returns instance of WindowManager.
        /// </summary>
        /// <returns></returns>
        public static WindowManager GetWindowManager()
        {
            return windowManager ?? (windowManager = new WindowManager());
        }


        /// <summary>
        /// Publishes screen state on UI Thread as a message. This message is handled in DisplayPanelViewModel.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="isActive"></param>
        public static void PublishScreenStateChange(ScreenControlEnum control, bool isActive)
        {
            GetEventAggregator().BeginPublishOnUIThread(new ScreenStateMessage
            {
                ScreenControl = control,
                ScreenState = isActive ? ScreenStateEnum.Active : ScreenStateEnum.Inactive
            });
        }
    }
}
