﻿namespace Common.Enums
{
    public enum ScreenControlEnum
    {
        AdditionalWindow,
        TabAContainer,
        TabBContainer,
        TabALeftPanel,
        TabARightPanel,
        DisplayPanel
    }
}
